public class Square {

	private int x_Axis;
	private int y_Axis;
	private Character occupant;
	private int guard;
	private int danger;
	private int potential;

	Square(int x, int y, int p) {
		x_Axis = x;
		y_Axis = y;
		occupant = null;
		guard = 0;
		danger = 0;
		potential = p;
	}

	public int getXPos() {
		return x_Axis;
	}

	public int getYPos() {
		return y_Axis;
	}

	public Character getOccupant() {
		return occupant;
	}

	public int getGuard() {
		return guard;
	}

	public int getDanger() {
		return danger;
	}

	public int getPotential() {
		return potential;
	}

	public void clear() {
		occupant = null;
	}

	public void setOccupant(Character o) {
		occupant = o;
	}
}
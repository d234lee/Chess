import java.util.List;
import java.util.ArrayList;

class King extends Character {
	private boolean inCheck = false;

	King(int x, int y, int p) {
		super(-1, x, y, p);
	}

	King(int x, int y, int p, boolean hasMoved) {
		super(-1, x, y, p);
		moved = hasMoved;
	}

	public boolean getInCheck() {
		return inCheck;
	}

	public void setInCheck(boolean check) {
		inCheck = check;
	}

	public ArrayList<ArrayList<Integer>> lr1(ChessBoard c) {
		Square currentSquare;
		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();

		if (y_Axis < 7) {
			currentSquare = c.getSquare(x_Axis, y_Axis + 1);
			coordinates.add(x_Axis);
			coordinates.add(y_Axis + 1);
			moves.add(coordinates);
		}
		if (y_Axis > 0) {
			coordinates = new ArrayList<Integer>();
			currentSquare = c.getSquare(x_Axis, y_Axis - 1);
			coordinates.add(x_Axis);
			coordinates.add(y_Axis - 1);
			moves.add(coordinates);
		}

		if (x_Axis < 7) {
			coordinates = new ArrayList<Integer>();
			currentSquare = c.getSquare(x_Axis + 1, y_Axis);
			coordinates.add(x_Axis + 1);
			coordinates.add(y_Axis);
			moves.add(coordinates);
			if (y_Axis < 7) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis + 1, y_Axis + 1);
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis + 1);
				moves.add(coordinates);
			}
			if (y_Axis > 0) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis + 1, y_Axis - 1);
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis - 1);
				moves.add(coordinates);
			}
		}

		if (x_Axis > 0) {
			coordinates = new ArrayList<Integer>();
			currentSquare = c.getSquare(x_Axis - 1, y_Axis);
			coordinates.add(x_Axis - 1);
			coordinates.add(y_Axis);
			moves.add(coordinates);
			if (y_Axis < 7) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis - 1, y_Axis + 1);
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis + 1);
				moves.add(coordinates);
			}
			if (y_Axis > 0) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis - 1, y_Axis - 1);
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis - 1);
				moves.add(coordinates);
			}
		}
		return moves;
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c) {
		Square currentSquare;
		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		if (y_Axis < 7) {
			currentSquare = c.getSquare(x_Axis, y_Axis + 1);
			coordinates.add(x_Axis);
			coordinates.add(y_Axis + 1);
			if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
				moves.add(coordinates);
			}
		}
		if (y_Axis > 0) {
			coordinates = new ArrayList<Integer>();
			currentSquare = c.getSquare(x_Axis, y_Axis - 1);
			coordinates.add(x_Axis);
			coordinates.add(y_Axis - 1);
			if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
				moves.add(coordinates);
			}
		}

		if (x_Axis < 7) {
			coordinates = new ArrayList<Integer>();
			currentSquare = c.getSquare(x_Axis + 1, y_Axis);
			coordinates.add(x_Axis + 1);
			coordinates.add(y_Axis);
			if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
				moves.add(coordinates);
			}
			if (y_Axis < 7) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis + 1, y_Axis + 1);
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis + 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					moves.add(coordinates);
				}
			}
			if (y_Axis > 0) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis + 1, y_Axis - 1);
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis - 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					moves.add(coordinates);
				}
			}
		}

		if (x_Axis > 0) {
			coordinates = new ArrayList<Integer>();
			currentSquare = c.getSquare(x_Axis - 1, y_Axis);
			coordinates.add(x_Axis - 1);
			coordinates.add(y_Axis);
			if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
				moves.add(coordinates);
			}
			if (y_Axis < 7) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis - 1, y_Axis + 1);
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis + 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					moves.add(coordinates);
				}
			}
			if (y_Axis > 0) {
				coordinates = new ArrayList<Integer>();
				currentSquare = c.getSquare(x_Axis - 1, y_Axis - 1);
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis - 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					moves.add(coordinates);
				}
			}
		}

		return moves;
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c, boolean checking) {
		ArrayList<ArrayList<Integer>> moves = possibleMoves(c);
		// castle move generation
		if (!moved && checking && c.isKingSafe(player)) {
			ArrayList<Integer> coordinates = new ArrayList<Integer>();
			if (c.getSquare(x_Axis - 1, y_Axis).getOccupant() == null &&
			c.getSquare(x_Axis - 2, y_Axis).getOccupant() == null &&
			c.getSquare(x_Axis - 3, y_Axis).getOccupant() == null &&
			!c.getSquare(x_Axis - 4, y_Axis).getOccupant().getMoved()) {
				Rook castlingRook = (Rook)c.getSquare(x_Axis - 4, y_Axis).getOccupant();
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis);
				moves.add(coordinates);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - 2);
				coordinates.add(y_Axis);
				moves.add(coordinates);
			}

			if (c.getSquare(x_Axis + 1, y_Axis).getOccupant() == null &&
			c.getSquare(x_Axis + 2, y_Axis).getOccupant() == null &&
			!c.getSquare(x_Axis + 3, y_Axis).getOccupant().getMoved()) {
				Rook castlingRook = (Rook)c.getSquare(x_Axis + 3, y_Axis).getOccupant();
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis);
				moves.add(coordinates);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + 2);
				coordinates.add(y_Axis);
				moves.add(coordinates);
			}
		}
		return moves;
	}
}
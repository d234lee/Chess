import java.util.List;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.lang.Math;

class ChessBoard implements Cloneable {
	Square[][] board;
	int length;
	ArrayList<Character> playerOne;
	King playerOneKing;
	ArrayList<Character> playerTwo;
	King playerTwoKing;
	Stack<HashMap<String, BitSet>> history;

	ChessBoard() {
		board = new Square[8][8];
		length = 8;
		playerOne = new ArrayList<Character>();
		playerTwo = new ArrayList<Character>();
		history = new Stack<HashMap<String, BitSet>>();
	}

	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	public Square getSquare(int x, int y) {
		if ((x >= 0) && (y >= 0) && (x < 8) && (y < 8)) {
			return board[y][x];
		}
		return null;
	}

	private static ArrayList<Character> cloneList(List<Character> list) {
    	ArrayList<Character> clone = new ArrayList<Character>(list.size());
    	try {
    		for (Character item : list) 
    			clone.add((Character)item.clone());
    	}
    	catch (CloneNotSupportedException e) {
    		System.out.println("BAD CLONE");
    	}
    	return clone;
	}

	public boolean checkMate(int player) {
		ArrayList<Character> pieces = cloneList(player == 1 ? playerOne : playerTwo);
		for (Character piece : pieces) {
			if (filterMoves(piece.getX(), piece.getY()).size() != 0)
				return false;
		}
		return true;
	}

	public boolean isKingSafe(int player) {
		ArrayList<Integer> kingsCoordinates = new ArrayList<Integer>();
		kingsCoordinates.add(player == 1 ? playerOneKing.getX() : playerTwoKing.getX());
		kingsCoordinates.add(player == 1 ? playerOneKing.getY() : playerTwoKing.getY());
		for (Character piece : player == 1 ? playerTwo : playerOne) {
			ArrayList<ArrayList<Integer>> moves = piece.possibleMoves(this);
			if (moves.contains(kingsCoordinates))
				return false;
		}
		return true;
	}

	public King getPlayersKing(int player) {
		return player == 1 ? playerOneKing : playerTwoKing;
	}

	public boolean removeCharacter(Character piece) {
		return piece.getPlayer() == 1 ? playerOne.remove(piece) : playerTwo.remove(piece);
	}

	public boolean addCharacter(Character piece) {
		return piece.getPlayer() == 1 ? playerOne.add(piece) : playerTwo.add(piece);
	}

	public void storeBoardState() {
		HashMap<String, BitSet> bitBoard = new HashMap<String, BitSet>();
		BitSet pawns1 = new BitSet(64);
		bitBoard.put("Pawn1", pawns1);
		BitSet pawnsMoved1 = new BitSet(64);
		bitBoard.put("PawnMoved1", pawnsMoved1);
		BitSet pawnsEnPassant1 = new BitSet(64);
		bitBoard.put("PawnEnPassant1", pawnsEnPassant1);
		BitSet knights1 = new BitSet(64);
		bitBoard.put("Knight1", knights1);
		BitSet bishops1 = new BitSet(64);
		bitBoard.put("Bishop1", bishops1);
		BitSet rooks1 = new BitSet(64);
		bitBoard.put("Rook1", rooks1);
		BitSet rooksMoved1 = new BitSet(64);
		bitBoard.put("RookMoved1", rooksMoved1);
		BitSet queens1 = new BitSet(64);
		bitBoard.put("Queen1", queens1);
		BitSet kings1 = new BitSet(64);
		bitBoard.put("King1", kings1);
		BitSet kingsMoved1 = new BitSet(64);
		bitBoard.put("KingMoved1", kingsMoved1);
		BitSet pawns2 = new BitSet(64);
		bitBoard.put("Pawn2", pawns2);
		BitSet pawnsMoved2 = new BitSet(64);
		bitBoard.put("PawnMoved2", pawnsMoved2);
		BitSet pawnsEnPassant2 = new BitSet(64);
		bitBoard.put("PawnEnPassant2", pawnsEnPassant2);
		BitSet knights2 = new BitSet(64);
		bitBoard.put("Knight2", knights2);
		BitSet bishops2 = new BitSet(64);
		bitBoard.put("Bishop2", bishops2);
		BitSet rooks2 = new BitSet(64);
		bitBoard.put("Rook2", rooks2);
		BitSet rooksMoved2 = new BitSet(64);
		bitBoard.put("RookMoved2", rooksMoved2);
		BitSet queens2 = new BitSet(64);
		bitBoard.put("Queen2", queens2);
		BitSet kings2 = new BitSet(64);
		bitBoard.put("King2", kings2);
		BitSet kingsMoved2 = new BitSet(64);
		bitBoard.put("KingMoved2", kingsMoved2);
		Character curr;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				curr = board[j][i].getOccupant();
				if (curr == null) continue;
				switch (curr.getClass().getName()) {
					case "Pawn": 
						Pawn currPawn = (Pawn)curr;
						if (curr.getPlayer() == 1 && currPawn.getEnPassant())
							pawnsEnPassant1.set(j * board.length + i);
						else if (curr.getPlayer() == 1 && curr.getMoved())
							pawnsMoved1.set(j * board.length + i);
						else if (curr.getPlayer() == 1)
							pawns1.set(j * board.length + i);
						else if (curr.getPlayer() ==  -1 && currPawn.getEnPassant())
							pawnsEnPassant2.set(j * board.length + i);
						else if (curr.getPlayer() == -1 && curr.getMoved())
							pawnsMoved2.set(j * board.length + i);
						else
							pawns2.set(j * board.length + i);
						break;
					case "Bishop": 
						if (curr.getPlayer() == 1)
							bishops1.set(j * board.length + i);
						else
							bishops2.set(j * board.length + i);
						break;
					case "Knight": 
						if (curr.getPlayer() == 1)
							knights1.set(j * board.length + i);
						else
							knights2.set(j * board.length + i);
						break;
					case "Rook": 
						if (curr.getPlayer() == 1 && curr.getMoved())
							rooksMoved1.set(j * board.length + i);
						else if (curr.getPlayer() == 1)
							rooks1.set(j * board.length + i);
						else if (curr.getPlayer() == -1 && curr.getMoved())
							rooksMoved2.set(j * board.length + i);
						else
							rooks2.set(j * board.length + i);
						break;
					case "Queen": 
						if (curr.getPlayer() == 1)
							queens1.set(j * board.length + i);
						else
							queens2.set(j * board.length + i);
						break;
					case "King": 
						if (curr.getPlayer() == 1 && curr.getMoved())
							kingsMoved1.set(j * board.length + i);
						else if (curr.getPlayer() == 1)
							kings1.set(j * board.length + i);
						else if (curr.getPlayer() == -1 && curr.getMoved())
							kingsMoved2.set(j * board.length + i);
						else
							kings2.set(j * board.length + i);
						break;
				} 
			}
		}
		history.push(bitBoard);
	}

	public void loadBoardState(HashMap<String, BitSet> boardState) {
		clearPieces();
		BitSet pawns1 = boardState.get("Pawn1");
		BitSet pawnsMoved1 = boardState.get("PawnMoved1");
		BitSet pawnsEnPassant1 = boardState.get("PawnEnPassant1");
		BitSet knights1 = boardState.get("Knight1");
		BitSet bishops1 = boardState.get("Bishop1");
		BitSet rooks1 = boardState.get("Rook1");
		BitSet rooksMoved1 = boardState.get("RookMoved1");
		BitSet queens1 = boardState.get("Queen1");
		BitSet kings1 = boardState.get("King1");
		BitSet kingsMoved1 = boardState.get("KingMoved1");
		BitSet pawns2 = boardState.get("Pawn2");
		BitSet pawnsMoved2 = boardState.get("PawnMoved2");
		BitSet pawnsEnPassant2 = boardState.get("PawnEnPassant2");
		BitSet knights2 = boardState.get("Knight2");
		BitSet bishops2 = boardState.get("Bishop2");
		BitSet rooks2 = boardState.get("Rook2");
		BitSet rooksMoved2 = boardState.get("RookMoved2");
		BitSet queens2 = boardState.get("Queen2");
		BitSet kings2 = boardState.get("King2");
		BitSet kingsMoved2 = boardState.get("KingMoved2");
		int count = pawns1.nextSetBit(0);
		while (count != -1) {
			Pawn newPawn = new Pawn(count % 8, (int)Math.floor(count / 8), 1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newPawn);
			playerOne.add(newPawn);
			count = pawns1.nextSetBit(++count);
		} 
		count = pawns2.nextSetBit(0);
		while (count != -1) {
			Pawn newPawn = new Pawn(count % 8, (int)Math.floor(count / 8), -1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newPawn);
			playerTwo.add(newPawn);
			count = pawns2.nextSetBit(++count);
		} 
		count = pawnsMoved1.nextSetBit(0);
		while (count != -1) {
			Pawn newPawn = new Pawn(count % 8, (int)Math.floor(count / 8), 1, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newPawn);
			playerOne.add(newPawn);
			count = pawnsMoved1.nextSetBit(++count);
		} 
		count = pawnsMoved2.nextSetBit(0);
		while (count != -1) {
			Pawn newPawn = new Pawn(count % 8, (int)Math.floor(count / 8), -1, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newPawn);
			playerTwo.add(newPawn);
			count = pawnsMoved2.nextSetBit(++count);
		} 
		count = pawnsEnPassant1.nextSetBit(0);
		while (count != -1) {
			Pawn newPawn = new Pawn(count % 8, (int)Math.floor(count / 8), 1, true, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newPawn);
			playerOne.add(newPawn);
			count = pawnsEnPassant1.nextSetBit(++count);
		} 
		count = pawnsEnPassant2.nextSetBit(0);
		while (count != -1) {
			Pawn newPawn = new Pawn(count % 8, (int)Math.floor(count / 8), -1, true, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newPawn);
			playerTwo.add(newPawn);
			count = pawnsEnPassant2.nextSetBit(++count);
		} 
		count = knights1.nextSetBit(0);
		while (count != -1) {
			Knight newKnight = new Knight(count % 8, (int)Math.floor(count / 8), 1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newKnight);
			playerOne.add(newKnight);
			count = knights1.nextSetBit(++count);
		} 
		count = knights2.nextSetBit(0);
		while (count != -1) {
			Knight newKnight = new Knight(count % 8, (int)Math.floor(count / 8), -1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newKnight);
			playerTwo.add(newKnight);
			count = knights2.nextSetBit(++count);
		} 
		count = bishops1.nextSetBit(0);
		while (count != -1) {
			Bishop newBishop = new Bishop(count % 8, (int)Math.floor(count / 8), 1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newBishop);
			playerOne.add(newBishop);
			count = bishops1.nextSetBit(++count);
		} 
		count = bishops2.nextSetBit(0);
		while (count != -1) {
			Bishop newBishop = new Bishop(count % 8, (int)Math.floor(count / 8), -1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newBishop);
			playerTwo.add(newBishop);
			count = bishops2.nextSetBit(++count);
		} 
		count = rooks1.nextSetBit(0);
		while (count != -1) {
			Rook newRook = new Rook(count % 8, (int)Math.floor(count / 8), 1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newRook);
			playerOne.add(newRook);
			count = rooks1.nextSetBit(++count);
		} 
		count = rooks2.nextSetBit(0);
		while (count != -1) {
			Rook newRook = new Rook(count % 8, (int)Math.floor(count / 8), -1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newRook);
			playerTwo.add(newRook);
			count = rooks2.nextSetBit(++count);
		} 
		count = rooksMoved1.nextSetBit(0);
		while (count != -1) {
			Rook newRook = new Rook(count % 8, (int)Math.floor(count / 8), 1, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newRook);
			playerOne.add(newRook);
			count = rooksMoved1.nextSetBit(++count);
		} 
		count = rooksMoved2.nextSetBit(0);
		while (count != -1) {
			Rook newRook = new Rook(count % 8, (int)Math.floor(count / 8), -1, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newRook);
			playerTwo.add(newRook);
			count = rooksMoved2.nextSetBit(++count);
		}
		count = queens1.nextSetBit(0);
		while (count != -1) {
			Queen newQueen = new Queen(count % 8, (int)Math.floor(count / 8), 1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newQueen);
			playerOne.add(newQueen);
			count = queens1.nextSetBit(++count);
		} 
		count = queens2.nextSetBit(0);
		while (count != -1) {
			Queen newQueen = new Queen(count % 8, (int)Math.floor(count / 8), -1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newQueen);
			playerTwo.add(newQueen);
			count = queens2.nextSetBit(++count);
		} 
		count = kings1.nextSetBit(0);
		while (count != -1) {
			King newKing = new King(count % 8, (int)Math.floor(count / 8), 1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newKing);
			playerOne.add(newKing);
			count = kings1.nextSetBit(++count);
			playerOneKing = newKing;
		} 
		count = kings2.nextSetBit(0);
		while (count != -1) {
			King newKing = new King(count % 8, (int)Math.floor(count / 8), -1);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newKing);
			playerTwo.add(newKing);
			count = kings2.nextSetBit(++count);
			playerTwoKing = newKing;
		}
		count = kingsMoved1.nextSetBit(0);
		while (count != -1) {
			King newKing = new King(count % 8, (int)Math.floor(count / 8), 1, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newKing);
			playerOne.add(newKing);
			count = kingsMoved1.nextSetBit(++count);
			playerOneKing = newKing;
		} 
		count = kingsMoved2.nextSetBit(0);
		while (count != -1) {
			King newKing = new King(count % 8, (int)Math.floor(count / 8), -1, true);
			board[(int)Math.floor(count / 8)][count % 8].setOccupant(newKing);
			playerTwo.add(newKing);
			count = kingsMoved2.nextSetBit(++count);
			playerTwoKing = newKing;
		} 
	}

	public void restoreLastState() {
		loadBoardState(history.peek());
	}

	private void clearPieces() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				board[j][i].setOccupant(null);
			}
		}
		playerOne.clear();
		playerTwo.clear();
		playerOneKing = null;
		playerTwoKing = null;
	}

	public ArrayList<ArrayList<Integer>> filterMoves(int x, int y) {
		Character currPiece = board[y][x].getOccupant();
		storeBoardState();
		ArrayList<ArrayList<Integer>> moves = currPiece.getClass().getName() == "King" ? 
		currPiece.possibleMoves(this, true) : currPiece.possibleMoves(this);
		ArrayList<ArrayList<Integer>> safeMoves = new ArrayList<ArrayList<Integer>>();
		for (ArrayList<Integer> currMove : moves) {
			currPiece.move(currMove.get(0), currMove.get(1), this);
			if (isKingSafe(currPiece.getPlayer()))
				safeMoves.add(currMove);
			restoreLastState();
			currPiece = board[y][x].getOccupant();
		}
		return safeMoves;
	}

	public void initializeGame() {
		board = new Square[8][8];
		int playerValue;
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				playerValue = j < 2 ? 1 : -1;
				board[j][i] = new Square(i, j, 0);
				if (j == 1 || j == 6) {
					Pawn newPawn = new Pawn(i, j, playerValue);
					board[j][i].setOccupant(newPawn);
					if (playerValue == 1) 
						playerOne.add(newPawn);
					else
						playerTwo.add(newPawn);
				}
				else if (j == 0 || j == 7) {
					if (i == 0 || i == 7) {
						Rook newRook = new Rook(i, j, playerValue);
						board[j][i].setOccupant(newRook);
						if (playerValue == 1) 
							playerOne.add(newRook);
						else
							playerTwo.add(newRook);
					} else if (i == 1 || i == 6) {
						Knight newKnight = new Knight(i, j, playerValue);
						board[j][i].setOccupant(newKnight);
						if (playerValue == 1) 
							playerOne.add(newKnight);
						else
							playerTwo.add(newKnight);
					} else if (i == 2 || i == 5) {
						Bishop newBishop = new Bishop(i, j, playerValue);
						board[j][i].setOccupant(newBishop);
						if (playerValue == 1) 
							playerOne.add(newBishop);
						else
							playerTwo.add(newBishop);
					} else if (i == 3) {
						Queen newQueen = new Queen(i, j, playerValue);
						board[j][i].setOccupant(newQueen);
						if (playerValue == 1) 
							playerOne.add(newQueen);
						else
							playerTwo.add(newQueen);
					} else {
						King newKing = new King(i, j, playerValue);
						board[j][i].setOccupant(newKing);
						if (playerValue == 1) {
							playerOne.add(newKing);
							playerOneKing = newKing;
						}
						else {
							playerTwo.add(newKing);
							playerTwoKing = newKing;
						}
					}
				}
			}
		}
	}


}
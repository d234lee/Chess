import java.util.ArrayList;
import java.util.List;

class Character implements Cloneable {
	protected int value;
	protected int x_Axis;
	protected int y_Axis;
	protected boolean moved;
	protected int player;

	Character() {
		value = 1;
		x_Axis = 0;
		y_Axis = 0;
		moved = false;
		player = 1;
	}

	Character(int v, int x, int y, int p) {
		value = v;
		x_Axis = x;
		y_Axis = y;
		moved = false;
		player = p;
	}

	private void setX(int x) {
		x_Axis = x;
	}

	public int getX() {
		return x_Axis;
	}

	private void setY(int y) {
		y_Axis = y;
	}

	public int getY() {
		return y_Axis;
	}

	public int getValue() {
		return value;
	}

	protected void changePos(int x, int y) {
		moved = true;

		setX(x);
		setY(y);
	}

	public int getPlayer() {
		return player;
	}

	public boolean getMoved() {
		return moved;
	}

	public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

	public ArrayList<ArrayList<Integer>> lr1(ChessBoard c) {
		return new ArrayList<ArrayList<Integer>>();
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c) {
		return new ArrayList<ArrayList<Integer>>();
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c, boolean checking) {
		return new ArrayList<ArrayList<Integer>>();
	}

	public boolean move(int x, int y, ChessBoard c) {
		ArrayList<Integer> givenMove = new ArrayList<Integer>();
		givenMove.add(x);
		givenMove.add(y);
		c.getSquare(x_Axis , y_Axis).setOccupant(null);
		if (c.getSquare(x, y).getOccupant() != null && 
			c.getSquare(x, y).getOccupant().getPlayer() != player) {
			c.removeCharacter(c.getSquare(x, y).getOccupant());
		}
		changePos(x, y);
		c.getSquare(x, y).setOccupant(this);
		moved = true;
		return true;
	}
}
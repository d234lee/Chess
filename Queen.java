import java.util.List;
import java.util.ArrayList;

class Queen extends Character {
	Queen(int x, int y, int p) {
		super(10, x, y ,p);
	}

	public ArrayList<ArrayList<Integer>> lr1(ChessBoard c) { // please optimize later
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		int count = 1;
		Square currentSquare;
		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		while (count < 8 - x_Axis && count < 8 - y_Axis) { // SouthEast
			currentSquare = c.getSquare(x_Axis + count, y_Axis + count);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + count);
				coordinates.add(y_Axis + count);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis + count);
			coordinates.add(y_Axis + count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < x_Axis + 1 && count < y_Axis + 1) { // NorthWest
			currentSquare = c.getSquare(x_Axis - count, y_Axis - count);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - count);
				coordinates.add(y_Axis - count);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis - count);
			coordinates.add(y_Axis - count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < y_Axis + 1 && count < 8 - x_Axis) { // SouthEast
			currentSquare = c.getSquare(x_Axis + count, y_Axis - count);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + count);
				coordinates.add(y_Axis - count);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis + count);
			coordinates.add(y_Axis - count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < x_Axis + 1 && count < 8 - y_Axis) {
			currentSquare = c.getSquare(x_Axis - count, y_Axis + count);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - count);
				coordinates.add(y_Axis + count);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis - count);
			coordinates.add(y_Axis + count);
			moves.add(coordinates);
			count++;
		}

		count = 1;
		coordinates = new ArrayList<Integer>();
		while (count < 8 - x_Axis) {
			currentSquare = c.getSquare(x_Axis + count, y_Axis);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + count);
				coordinates.add(y_Axis);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis + count);
			coordinates.add(y_Axis);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < x_Axis + 1) {
			currentSquare = c.getSquare(x_Axis - count, y_Axis);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - count);
				coordinates.add(y_Axis);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis - count);
			coordinates.add(y_Axis);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < 8 - y_Axis) {
			currentSquare = c.getSquare(x_Axis, y_Axis + count);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis);
				coordinates.add(y_Axis + count);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis);
			coordinates.add(y_Axis + count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < y_Axis + 1) {
			currentSquare = c.getSquare(x_Axis, y_Axis - count);
			if (currentSquare.getOccupant() != null) {
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis);
				coordinates.add(y_Axis - count);
				moves.add(coordinates);
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis);
			coordinates.add(y_Axis - count);
			moves.add(coordinates);
			count++;
		}

		return moves;
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c) { // please optimize later
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		int count = 1;
		Square currentSquare;
		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		while (count < 8 - x_Axis && count < 8 - y_Axis) { // SouthEast
			currentSquare = c.getSquare(x_Axis + count, y_Axis + count);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis + count);
					coordinates.add(y_Axis + count);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis + count);
			coordinates.add(y_Axis + count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < x_Axis + 1 && count < y_Axis + 1) { // NorthWest
			currentSquare = c.getSquare(x_Axis - count, y_Axis - count);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - count);
					coordinates.add(y_Axis - count);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis - count);
			coordinates.add(y_Axis - count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < y_Axis + 1 && count < 8 - x_Axis) { // SouthEast
			currentSquare = c.getSquare(x_Axis + count, y_Axis - count);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis + count);
					coordinates.add(y_Axis - count);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis + count);
			coordinates.add(y_Axis - count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < x_Axis + 1 && count < 8 - y_Axis) {
			currentSquare = c.getSquare(x_Axis - count, y_Axis + count);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - count);
					coordinates.add(y_Axis + count);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis - count);
			coordinates.add(y_Axis + count);
			moves.add(coordinates);
			count++;
		}

		count = 1;
		coordinates = new ArrayList<Integer>();
		while (count < 8 - x_Axis) {
			currentSquare = c.getSquare(x_Axis + count, y_Axis);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis + count);
					coordinates.add(y_Axis);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis + count);
			coordinates.add(y_Axis);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < x_Axis + 1) {
			currentSquare = c.getSquare(x_Axis - count, y_Axis);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - count);
					coordinates.add(y_Axis);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis - count);
			coordinates.add(y_Axis);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < 8 - y_Axis) {
			currentSquare = c.getSquare(x_Axis, y_Axis + count);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis);
					coordinates.add(y_Axis + count);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis);
			coordinates.add(y_Axis + count);
			moves.add(coordinates);
			count++;
		}
		count = 1;
		while (count < y_Axis + 1) {
			currentSquare = c.getSquare(x_Axis, y_Axis - count);
			if (currentSquare.getOccupant() != null) {
				if (currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis);
					coordinates.add(y_Axis - count);
					moves.add(coordinates);
				}
				break;
			}
			coordinates = new ArrayList<Integer>();
			coordinates.add(x_Axis);
			coordinates.add(y_Axis - count);
			moves.add(coordinates);
			count++;
		}

		return moves;
	}
}
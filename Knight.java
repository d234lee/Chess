import java.util.List;
import java.util.ArrayList;

class Knight extends Character {
	Knight(int x, int y, int p) {
		super(3, x, y, p);
	}

	public ArrayList<ArrayList<Integer>> lr1(ChessBoard c) {
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		Square currentSquare;

		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		if (x_Axis < 7) {
			if (y_Axis < 6) {
				currentSquare = c.getSquare(x_Axis + 1, y_Axis + 2);
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis + 2);
				moves.add(coordinates);
			}
			if (y_Axis > 1) {
				currentSquare = c.getSquare(x_Axis + 1, y_Axis - 2);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + 1);
				coordinates.add(y_Axis - 2);
				moves.add(coordinates);
			}
		}

		if (x_Axis > 0) {
			if (y_Axis < 6) {
				currentSquare = c.getSquare(x_Axis - 1, y_Axis + 2);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis + 2);
				moves.add(coordinates);
			}
			if (y_Axis > 1) {
				currentSquare = c.getSquare(x_Axis - 1, y_Axis - 2);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - 1);
				coordinates.add(y_Axis - 2);
				moves.add(coordinates);
			}
		}

		if (y_Axis < 7) {
			if (x_Axis < 6) {
				currentSquare = c.getSquare(x_Axis + 2, y_Axis + 1);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + 2);
				coordinates.add(y_Axis + 1);
				moves.add(coordinates);
			}
			if (x_Axis > 1) {
				currentSquare = c.getSquare(x_Axis - 2, y_Axis + 1);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - 2);
				coordinates.add(y_Axis + 1);
				moves.add(coordinates);
			}
		}

		if (y_Axis > 0) {
			if (x_Axis < 6) {
				currentSquare = c.getSquare(x_Axis + 2, y_Axis - 1);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis + 2);
				coordinates.add(y_Axis - 1);
				moves.add(coordinates);
			}
			if (x_Axis > 1) {
				currentSquare = c.getSquare(x_Axis - 2, y_Axis - 1);
				coordinates = new ArrayList<Integer>();
				coordinates.add(x_Axis - 2);
				coordinates.add(y_Axis - 1);
				moves.add(coordinates);
			}
		}
		return moves;
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c) {
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		Square currentSquare;

		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		if (x_Axis < 7) {
			if (y_Axis < 6) {
				currentSquare = c.getSquare(x_Axis + 1, y_Axis + 2);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates.add(x_Axis + 1);
					coordinates.add(y_Axis + 2);
					moves.add(coordinates);
				}
			}
			if (y_Axis > 1) {
				currentSquare = c.getSquare(x_Axis + 1, y_Axis - 2);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis + 1);
					coordinates.add(y_Axis - 2);
					moves.add(coordinates);
				}
			}
		}

		if (x_Axis > 0) {
			if (y_Axis < 6) {
				currentSquare = c.getSquare(x_Axis - 1, y_Axis + 2);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - 1);
					coordinates.add(y_Axis + 2);
					moves.add(coordinates);
				}
			}
			if (y_Axis > 1) {
				currentSquare = c.getSquare(x_Axis - 1, y_Axis - 2);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - 1);
					coordinates.add(y_Axis - 2);
					moves.add(coordinates);
				}
			}
		}

		if (y_Axis < 7) {
			if (x_Axis < 6) {
				currentSquare = c.getSquare(x_Axis + 2, y_Axis + 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis + 2);
					coordinates.add(y_Axis + 1);
					moves.add(coordinates);
				}
			}
			if (x_Axis > 1) {
				currentSquare = c.getSquare(x_Axis - 2, y_Axis + 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - 2);
					coordinates.add(y_Axis + 1);
					moves.add(coordinates);
				}
			}
		}

		if (y_Axis > 0) {
			if (x_Axis < 6) {
				currentSquare = c.getSquare(x_Axis + 2, y_Axis - 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis + 2);
					coordinates.add(y_Axis - 1);
					moves.add(coordinates);
				}
			}
			if (x_Axis > 1) {
				currentSquare = c.getSquare(x_Axis - 2, y_Axis - 1);
				if (currentSquare.getOccupant() == null || currentSquare.getOccupant().getPlayer() != player) {
					coordinates = new ArrayList<Integer>();
					coordinates.add(x_Axis - 2);
					coordinates.add(y_Axis - 1);
					moves.add(coordinates);
				}
			}
		}
		return moves;
	}
}
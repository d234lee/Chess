import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JComponent;
import java.awt.BasicStroke;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import java.util.Scanner;


class ChessGame extends JPanel implements ActionListener {
	ChessBoard gameBoard = new ChessBoard();
	ArrayList<ArrayList<Integer>> highlighted = new ArrayList<ArrayList<Integer>>();
	Square currentSquare;
	public JFrame jFrameObj = new JFrame();
	public static final int SQUARE_LENGTH = 75;
	public static final int BOARD_LENGTH = 8;
	public static final int SCREEN_LENGTH = 625;
	public static final int SCREEN_WIDTH = 600;
	public static int PLAYER_TURN = -1;

	public void paintComponent(Graphics g) {
		super.paintComponent(g);     // paint parent's background
		setBackground(Color.WHITE);  // set background color for this JPanel
		// Your custom painting codes. For example,
		// Drawing primitive shapes // set the drawing color
		for (int i = 0; i < BOARD_LENGTH; i++) {
			for (int j = 0; j < BOARD_LENGTH; j++) {
				g.setColor(Color.BLACK);
				ArrayList<Integer> a = new ArrayList<Integer>();
				a.add(i);
				a.add(7 - j);
				if ((i + j) % 2 != 0) {
					g.setColor(new Color(156, 93, 82));
				}
				else {
					g.setColor(Color.WHITE);
				}
				g.fillRect(i * SQUARE_LENGTH, j * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);

				if (highlighted.contains(a)) {
					g.setColor(Color.GREEN);
					Graphics2D g2d = (Graphics2D) g;
					g2d.setStroke(new BasicStroke(4));
					g.drawRect(i * SQUARE_LENGTH, j * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
				}

				Square s = gameBoard.getSquare(i, 7 - j);
				if (s.getOccupant() != null) {
                    if (s.getOccupant().getClass().getName() == "Pawn" && (7 - j) % 7 == 0) {
                        boolean invalidInput = true;
                        Character upgraded = new Character();
                        while (invalidInput) {
                            Scanner reader = new Scanner(System.in);
                            System.out.println("Pawn " + i + ", " + (7 - j) + " to: Knight (K), Bishop (B), Rook (R), Queen (Q)");
                            String input = reader.nextLine();
                            switch (input) {
                                case "K": upgraded = new Knight(i, 7 - j, s.getOccupant().getPlayer()); invalidInput = false; break;
                                case "B": upgraded = new Bishop(i, 7 - j, s.getOccupant().getPlayer()); invalidInput = false; break;
                                case "R": upgraded = new Rook(i, 7 - j, s.getOccupant().getPlayer(), true); invalidInput = false; break;
                                case "Q": upgraded = new Queen(i, 7 - j, s.getOccupant().getPlayer()); invalidInput = false; break;
                                default: System.out.println("Invalid input. Try again.");
                            }
                        }       
                        gameBoard.removeCharacter(s.getOccupant());
                        s.setOccupant(upgraded);
                        gameBoard.addCharacter(upgraded);
                    }
					g.setColor(s.getOccupant().getPlayer() == 1 ? Color.BLACK : Color.decode("#551A8B"));
					String className = s.getOccupant().getClass().getName();
					if (className == "Pawn") {
						g.drawString("PAWN", i * SQUARE_LENGTH + 20, j * SQUARE_LENGTH + 45);
					} else if (className == "Bishop") {
						g.drawString("BISHOP", i * SQUARE_LENGTH + 20, j * SQUARE_LENGTH + 45);
					} else if (className == "Knight") {
						g.drawString("KNIGHT", i * SQUARE_LENGTH + 20, j * SQUARE_LENGTH + 45);
					} else if (className == "Rook") {
						g.drawString("ROOK", i * SQUARE_LENGTH + 20, j * SQUARE_LENGTH + 45);
					} else if (className == "Queen") {
						g.drawString("QUEEN", i * SQUARE_LENGTH + 20, j * SQUARE_LENGTH + 45);
					} else if (className == "King") {
						g.drawString("KING", i * SQUARE_LENGTH + 20, j * SQUARE_LENGTH + 45);
					}
				}
			}
		}

        for (Character piece : PLAYER_TURN == -1 ? gameBoard.playerOne : gameBoard.playerTwo) {
            if (piece.getClass().getName() == "Pawn") {
                Pawn p = (Pawn) piece;
                p.setEnPassant(false);
            }
        }

        // checkmate check
        if (gameBoard.checkMate(PLAYER_TURN * -1)) {
            JOptionPane.showMessageDialog(null, "Checkmate!!");
            gameBoard.initializeGame();
            this.repaint();
        }
	}

	public void actionPerformed(ActionEvent evt) {
		Object src = evt.getSource();
		String coords = ((JComponent) src).getName();
		int xCoord = coords.charAt(0) - 48;
		int yCoord = coords.charAt(1) - 48;
		Square s = gameBoard.getSquare(xCoord, yCoord);
		Character currPiece;
		ArrayList<Integer> coordinates = new ArrayList<Integer>();
		coordinates.add(xCoord);
		coordinates.add(yCoord);
		if (highlighted.contains(coordinates)) {
			gameBoard.getSquare(currentSquare.getXPos(), currentSquare.getYPos())
			.getOccupant().move(xCoord, yCoord, gameBoard);
			highlighted = new ArrayList<ArrayList<Integer>>();
			PLAYER_TURN *= -1;
		}
		else if (s.getOccupant() == null || s.getOccupant().getPlayer() == PLAYER_TURN) {
			//System.out.println("No Piece");
		}
		else {
			highlighted = gameBoard.filterMoves(xCoord, yCoord);
			currentSquare = s;
		}

		this.repaint();
	}

	public static void main(String[] args) {
		ChessGame mainGame = new ChessGame();
		mainGame.gameBoard.initializeGame();
		JFrame frame = mainGame.jFrameObj;
		frame.setSize(SCREEN_WIDTH , SCREEN_LENGTH);
		JButton[][] buttons = new JButton[BOARD_LENGTH][BOARD_LENGTH];
		for (int i = 0; i < BOARD_LENGTH; i++) {
			JButton[] row = new JButton[BOARD_LENGTH];
			for (int j = 0; j < BOARD_LENGTH; j++) {
				JButton button = new JButton();
				button.setName("" + j + i);
				button.setBounds(j * SQUARE_LENGTH, 525 - i * SQUARE_LENGTH, SQUARE_LENGTH, SQUARE_LENGTH);
				button.setOpaque(false);
				button.setContentAreaFilled(false);
				button.setBorderPainted(false);
				button.addActionListener(mainGame);
				frame.add(button);
				row[j] = button;
			}
			buttons[i] = row;
		}
		frame.getContentPane().add(mainGame);
		frame.setLocationRelativeTo(null);
		frame.setBackground(Color.LIGHT_GRAY);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

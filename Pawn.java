import java.util.List;
import java.util.ArrayList;


class Pawn extends Character {
	boolean enPassant = false;
	int enPassantState = 0;
	Pawn(int x, int y, int p) {
		super(1, x, y, p);
	}

	Pawn(int x, int y, int p, boolean hasMoved) {
		super(1, x, y, p);
		moved = hasMoved;
	}

	Pawn(int x, int y, int p, boolean hasMoved, boolean inEnPassant) {
		super(1, x, y, p);
		moved = hasMoved;
		enPassant = inEnPassant;
	}

	public boolean getEnPassant() {
		return enPassant;
	}

	public void setEnPassant(boolean val) {
		enPassant = val;
	}

	public ArrayList<ArrayList<Integer>> lr1(ChessBoard c) {
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		int pos_X = x_Axis;
		int pos_Y = y_Axis;
		Square currentSquare = c.getSquare(pos_X, pos_Y + 1 * player);
		ArrayList<Integer> b;
		if (player == 1 ? pos_Y == 7 : pos_Y == 0) 
			return moves; // never should occur

		currentSquare = c.getSquare(pos_X, pos_Y + 2 * player);
		
		if (pos_X > 0) {
			currentSquare = c.getSquare(pos_X - 1, pos_Y + 1 * player);
			b = new ArrayList<Integer>(); // take left piece
			b.add(pos_X - 1);
			b.add(pos_Y + 1 * player);
			moves.add(b);
		}

		if (pos_X < 7) {
			currentSquare = c.getSquare(pos_X + 1, pos_Y + 1 * player);
			b = new ArrayList<Integer>(); // take right piece
			b.add(pos_X + 1);
			b.add(pos_Y + 1 * player);
			moves.add(b);
		}
		return moves;
	}

	public ArrayList<ArrayList<Integer>> possibleMoves(ChessBoard c) {
		ArrayList<ArrayList<Integer>> moves = new ArrayList<ArrayList<Integer>>();
		int pos_X = x_Axis;
		int pos_Y = y_Axis;
		Square currentSquare = c.getSquare(pos_X, pos_Y + 1 * player);
		ArrayList<Integer> b;
		if (player == 1 ? pos_Y == 7 : pos_Y == 0) 
			return moves; // never should occur

		if (currentSquare.getOccupant() == null) { // one space forward
			b = new ArrayList<Integer>();
			b.add(pos_X);
			b.add(pos_Y + 1 * player);
			moves.add(b);
			if (!moved && currentSquare.getOccupant() == null && // two spaces forward
				c.getSquare(pos_X, pos_Y + 2 * player).getOccupant() == null) {
				b = new ArrayList<Integer>();
				b.add(pos_X);
				b.add(pos_Y + 2 * player);
				moves.add(b);
			}
		}

		currentSquare = c.getSquare(pos_X, pos_Y + 2 * player);
		
		if (pos_X > 0) {
			currentSquare = c.getSquare(pos_X - 1, pos_Y + 1 * player);
			if (currentSquare.getOccupant() != null && currentSquare.getOccupant().getPlayer() != player) {
				b = new ArrayList<Integer>(); // take left piece
				b.add(pos_X - 1);
				b.add(pos_Y + 1 * player);
				moves.add(b);
			}

			currentSquare = c.getSquare(pos_X - 1, pos_Y); // en passant check NEED TO ADD RULE WHERE MUST TAKE IMMEDIATELY
			Character currentCharacter = currentSquare.getOccupant();
			if (currentCharacter != null && currentCharacter.getPlayer() != player && 
				currentCharacter.getValue() == 1) {
				Pawn currentPawn = (Pawn) currentCharacter;
				if (currentPawn.getEnPassant()) {
					b = new ArrayList<Integer>();
					b.add(pos_X - 1);
					b.add(pos_Y + 1 * player);
					moves.add(b);
				}
			}
		}

		if (pos_X < 7) {
			currentSquare = c.getSquare(pos_X + 1, pos_Y + 1 * player);
			if (currentSquare.getOccupant() != null && currentSquare.getOccupant().getPlayer() != player) {
				b = new ArrayList<Integer>(); // take right piece
				b.add(pos_X + 1);
				b.add(pos_Y + 1 * player);
				moves.add(b);
			}

			currentSquare = c.getSquare(pos_X + 1, pos_Y);
			Character currentCharacter = currentSquare.getOccupant();
			if (currentCharacter != null && currentCharacter.getPlayer() != player && 
				currentCharacter.getValue() == 1) {
				Pawn currentPawn = (Pawn) currentCharacter;
				if (currentPawn.getEnPassant()) {
					b = new ArrayList<Integer>();
					b.add(pos_X + 1);
					b.add(pos_Y + 1 * player);
					moves.add(b);
				}
			}
		}
		return moves;
	}

	public boolean move(int x, int y, ChessBoard c) {
		ArrayList<Integer> givenMove = new ArrayList<Integer>();
		givenMove.add(x);
		givenMove.add(y);

		if (!moved && (y - y_Axis) * player == 2 && x == x_Axis)
			enPassant = true;
		else 
			enPassant = false;

		if (x_Axis != x && c.getSquare(x, y).getOccupant() == null) { // en passant
			c.getSquare(x, y - 1 * player).setOccupant(null);
		}

		if (c.getSquare(x, y).getOccupant() != null && 
			c.getSquare(x, y).getOccupant().getPlayer() != player) {
			c.removeCharacter(c.getSquare(x, y).getOccupant());
		}

		c.getSquare(x_Axis , y_Axis).setOccupant(null);
		changePos(x, y);
		c.getSquare(x, y).setOccupant(this);
		
		return true;
	}
}